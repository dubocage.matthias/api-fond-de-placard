<?php

namespace App\Validators;

class Validator
{
    private $data;
    private $error;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * 
     */
    public function validate(array $rules) : ?array
    {
        foreach($rules as $name => $rulesArray)
        {
            if(array_key_exists($name,$this->data))
            {
                foreach($rulesArray as $rule)
                {
                    switch($rule)
                    {
                        case 'required':
                            $this->required($name,$this->data[$name]);
                            break;
                        default :
                            // ------
                            break;
                    }
                }
            }
        }
        return $this->error;
    }

    /**
     * 
     */
    private function required(string $name, string $value)
    {
        $value = trim($value);

        if(!isset($value) || empty($value) || is_null($value)){
            $this->error[$name][]= '{$name} est requis.';
        }
    } 
}