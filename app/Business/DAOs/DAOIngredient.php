<?php

namespace App\Business\DAOs;
use App\Business\Models\Ingredient;

class DAOIngredient extends DAOModel
{
    /**
     * 
     */
    public function chargeIngredient(Ingredient $ingredient)
    {
        $sql ="
            SELECT * 
            FROM ingredient 
            WHERE id = :id
        ";
        $values =[
            [
                'param' => ':id',
                'value' => $ingredient->getId(),
                'type'  => 'int'
            ]
        ];

        $ingredient->hydrate($this->db->prepare($sql,$values,'fetch'));
    }

    /**
     * 
     */
    public function insertIngredient(Ingredient $ingredient):bool
    {
        $sql ="
            INSERT INTO ingredient ( name)
            VALUES ( :name )
        ";
        $values =[
            [
                'param' => ':name',
                'value' => $ingredient->getName(),
                'type'  => 'string'
            ]
        ];

        $ingredient->setId($this->db->prepare($sql,$values,'fetchAll',true)['id']);

        return is_null($ingredient->getId()) ? false : true;
    }
    /**
     * 
     */
    function getAllIngredient():array
    {
        $ingredients = [];

        foreach($this->db->query('SELECT * FROM ingredient','fetchAll') as $data)
        {
            $ingredient = new Ingredient();
            $ingredient->hydrate($data);
            $ingredients[] = $ingredient;
        }

        return $ingredients;
    }

    /**
     * 
     */
    function getIngredientWithSameName(string $name): Ingredient 
    {
        $ingredient = new Ingredient();
        $ingredient->hydrate(   $this->db->prepare('SELECT * FROM ingredient WHERE name = :name',
                                [   
                                    [
                                        'param' => ':name',
                                        'value' => $name,
                                        'type'  => 'string'
                                    ]
                                    ],'fetch')        
        );
        
        return $ingredient;
    }

    /**
     * 
     */
    function getIngredientsByName(string $name): array 
    {    
        $ingredients = [];

        foreach(    
                    $this->db->prepare( 'SELECT * FROM ingredient WHERE name LIKE :name',
                                        [   
                                            [
                                                'param' => ':name',
                                                'value' => $name,
                                                'type'  => 'string'
                                            ]
                                            ],'fetchAll'
                    ) as $data
                )
        {
            $ingredients[] = (new Ingredient())->hydrate($data);
        }
        
        return $ingredients;
    }

    /**
     * 
     */
    public function getIngredientsByIdRecipe(int $id) : array
    {
        $ingredients = [];

        $sql =  "   
            SELECT i.* 
            FROM ingredient i
                INNER JOIN recipe_ingredient ri ON ri.ingredient_id = i.id
                INNER JOIN recipe r ON r.id = ri.recipe_id
            WHERE r.id = :id
        ";
        $values = [   
            [
                'param' => ':id',
                'value' => $id,
                'type'  => 'int'
            ]
        ];            
    
        foreach($this->db->prepare($sql,$values,'fetchAll') as $data)
        {
            $ingredient = new Ingredient();
            $ingredient->hydrate($data);
            $ingredients[] = $ingredient;
        }

        return $ingredients;
    }
}