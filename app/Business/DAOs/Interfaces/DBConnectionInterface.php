<?php

namespace App\Business\DAOs\Interfaces;

interface DBConnectionInterface
{
    public function query(string $query, string $fetch = 'fetchAll');
    public function prepare(string $query, array $values, string $fetch = 'fetchAll', bool $insert = false);
}