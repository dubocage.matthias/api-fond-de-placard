<?php

namespace App\Business\DAOs;
use App\Business\Models\Recipe;

class DAORecipe extends DAOModel
{
    /**
     * 
     */
    public function chargeRecipe(Recipe $recipe) : bool
    {
        $sql="
            SELECT * 
            FROM recipe 
            WHERE id = :id 
        ";
        $values = [
            [
                'param' => ':id',
                'value' => $recipe->getId(),
                'type'  => 'string'
            ]
        ];

        $recipe->hydrate($this->db->prepare($sql,$values,'fetch'));

        return true;
    }

    /**
     * 
     */
    public function updateRecipe(Recipe $recipe) : bool
    {
        $sql="
            UPDATE recipe
            SET  name = :name , category = :category , picture = :picture ,score = :score, nbScore = :nbScore
            WHERE id = :id
        ";
        $values = [
            [
                'param' => ':id',
                'value' => $recipe->getId(),
                'type'  => 'int'
            ],
            [
                'param' => ':name',
                'value' => $recipe->getName(),
                'type'  => 'string'
            ],
            [
                'param' => ':category',
                'value' => $recipe->getCategory(),
                'type'  => 'string'
            ],
            [
                'param' => ':picture',
                'value' => $recipe->getPicture(),
                'type'  => 'string'
            ],
            [
                'param' => ':score',
                'value' => $recipe->getScore(),
                'type'  => 'float'
            ],
            [
                'param' => ':nbScore',
                'value' => $recipe->getNbScore(),
                'type'  => 'float'
            ],
        ];

        $this->db->prepare($sql,$values,'fetchAll');

        return true;
    }

    /**
     * 
     */
    public function insertRecipe(Recipe $recipe) : bool
    {
        $sql ="
            INSERT INTO recipe ( name, category, picture, score, :nbScore)
            VALUES ( :name , :category, :picture, :score , :nbScore)
        ";
        $values =[
            [
                'param' => ':name',
                'value' => $recipe->getName(),
                'type'  => 'string'
            ],
            [
                'param' => ':category',
                'value' => $recipe->getCategory(),
                'type'  => 'string'
            ],
            [
                'param' => ':picture',
                'value' => $recipe->getPicture(),
                'type'  => 'string'
            ],
            [
                'param' => ':score',
                'value' => $recipe->getScore(),
                'type'  => 'float'
            ],
            [
                'param' => ':nbScore',
                'value' => $recipe->getNbScore(),
                'type'  => 'int'
            ],
        ];

        $recipe->setId($this->db->prepare($sql,$values,'fetch',true)['id']);

        return is_null($recipe->getId()) ? false : true;
    }
    
    /**
     * 
     */
    function getAllRecipe() : array
    {
        $recipes = [];

        foreach($this->db->query('SELECT * FROM recipe','fetchAll') as $data)
        {
            $recipe = new Recipe();
            $recipe->hydrate($data);
            $recipes[] = $recipe;
        }

        return $recipes;
    }

    /**
     * 
     */
    function getRecipeSameName(string $name) : recipe 
    {
        $recipe = new Recipe();
        $recipe->hydrate(   $this->db->prepare('SELECT * FROM recipe WHERE name = :name',
                                [   
                                    [
                                        'param' => ':name',
                                        'value' => $name,
                                        'type'  => 'string'
                                    ]
                                ] ,'fetchAll')      
        );
        
        return $recipe;
    }

    /**
     * 
     */
    function getRecipeByName(string $name) : array 
    {
        $recipes = [];

        foreach(    
                    $this->db->prepare( 'SELECT * FROM recipe WHERE name LIKE :name',
                                        [   
                                            [
                                                'param' => ':name',
                                                'value' => $name,
                                                'type'  => 'string'
                                            ]
                                            ],'fetchAll'
                    ) as $data
                )
        {
            $recipes[] = (new Recipe())->hydrate($data);
        }
        
        return $recipes;
    }

    /**
     * 
     */
    public function getRecipesByIdIngredient(int $id) : array
    {
        $recipes = [];

        $sql =  "   
            SELECT r.* 
            FROM recipe r
                INNER JOIN recipe_ingredient ri ON ri.recipe_id = r.id
                INNER JOIN ingredient i ON i.id = ri.ingredient_id
            WHERE i.id = :id
        ";
        $values =    [ 
            [ 
                'param' => ':id',
                'value' => $id,
                'type'  => 'int'
            ]
        ];            

                
        foreach($this->db->prepare($sql,$values,'fetchAll') as $data)
        {
            $recipe = new Recipe();
            $recipe->hydrate($data);
            $recipes[] = $recipe;
        }

        return $recipes;
    }

    /**
     * 
     */
    public function ingredientsLinkedToRecipe(array $ingredients,Recipe $recipe) : bool
    {
        $sql =  "   
            INSERT INTO recipe_ingredient (recipe_id,ingredient_id)
            VALUE (:recipe_id , :ingredient_id)
        ";
        
        foreach($ingredients as $ingredient)
        {
            $values =    [ 
                [ 
                    'param' => ':recipe_id',
                    'value' => $recipe->getId(),
                    'type'  => 'int'
                ], 
                [ 
                    'param' => ':ingredient_id',
                    'value' => $ingredient->getId(),
                    'type'  => 'int'
                ]
            ];
            if(is_null($this->db->prepare($sql,$values,'fetchAll',true))){
                return false;
            }
        }
        return true;
    }

    /**
     * 
     */
    public function search(string $word) : array
    {
        $sql =  "   
            SELECT DISTINCT r.* 
            FROM recipe r
                INNER JOIN recipe_ingredient ri ON ri.recipe_id = r.id
                INNER JOIN ingredient i ON i.id = ri.ingredient_id
            WHERE
                    r.name LIKE :search 
                OR  i.name LIKE :search
                OR  r.category LIKE :search     
        ";
        $values =    [ 
            [ 
                'param' => ':search',
                'value' => '%'.$word.'%',
                'type'  => 'string'
            ]
        ];

        $recipes = [];

        foreach($this->db->prepare($sql,$values,'fetchAll') as $result)
        {
            $recipe = new Recipe();
            $recipe->hydrate($result);
            $recipes[] = $recipe;
        }

        return  $recipes;
    }

    /**
     * 
     */
    public function deletedLinksRecipeIngredient(Recipe $recipe) : bool
    {
        $sql="
            DELETE
            FROM   recipe_ingredient
            WHERE  recipe_id = :recipe_id  
        ";
        $values =    [ 
            [ 
                'param' => ':recipe_id',
                'value' => $recipe->getId(),
                'type'  => 'int'
            ]
        ];

        $this->db->prepare($sql,$values,'fetchAll');

        return true;
    }
}