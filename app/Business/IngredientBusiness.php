<?php

namespace App\Business;

use App\Business\DAOs\DAOIngredient;
use App\Business\DAOs\DAORecipe;
use App\Business\Models\Ingredient;
use App\Business\Models\Recipe;

class IngredientBusiness extends ModelBusiness
{
    /**
     * 
     */
    function getAll():array
    {
        $daoIngredient = new DAOIngredient($this->db);

        return $daoIngredient->getAllIngredient();
    }

    /**
     * 
     */
    function insert(array $data)
    {
        $daoIngredient = new DAOIngredient($this->db);
        $ingredient = new Ingredient();

        $ingredient->hydrate($data);
        $daoIngredient->insertIngredient($ingredient);
    }

    /**
     * 
     */
    function getRecipesByIdIngredient(int $id): array
    {
        $datas =[ 'ingredient' => null , 'recipes' => null];

        $ingredient = new Ingredient($id);

        $daoIngredient = new DAOIngredient($this->db);
        $daoRecipe = new DAORecipe($this->db);

        $daoIngredient->chargeIngredient($ingredient);
        $recipes = $daoRecipe->getRecipesByIdIngredient($id) ;

        $datas['ingredient'] = $ingredient;
        $datas['recipes'] = $recipes;
        
        return $datas;
    }

    /**
     * 
     */
    public function getRecipesByListIdIngredient(array $datas) : array
    {
        $daoRecipe = new DAORecipe($this->db);

        if(empty($datas['ingredients']) || is_null($datas['ingredients']) || count($datas['ingredients']) === 0 )
        {
            return $daoRecipe->getAllRecipe();
        }
        
        $ingredients = [];

        foreach($datas['ingredients'] as $data)
        {
            foreach ($daoRecipe->getRecipesByIdIngredient($data) as $newIngredient)
            {
                $contain = false;
                foreach($ingredients as $ingredient)
                {
                    if($ingredient->getId() === $newIngredient->getId())
                    {
                        $contain = true;
                    }
                }

                if($contain === false){
                    $ingredients[] = $newIngredient;
                }  
            }
        }

        return $ingredients;
    }
}