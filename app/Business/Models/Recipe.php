<?php

namespace App\Business\Models;

class Recipe extends Model
{
    private $name;
    private $category;
    private $picture = '';
    private $score = 0.0;
    private $nbScore = 0 ;

    function getName():string{
        return $this->name;
    }

    function setName(string $name){
        return $this->name = $name;
    }
    
    function getCategory():string{
        return $this->category;
    }

    function setCategory(string $category){
        return $this->category = $category;
    }

    function getPicture():string{
        return $this->picture;
    }

    function setPicture(string $picture){
        return $this->picture = $picture;
    }

    function getScore():float{
        return $this->score;
    }

    function setScore(float $score){
        return $this->score = $score;
    }

    function getNbScore() : int{
        return $this->nbScore ;
    }

    function setNbScore(int $nb){
        $this->nbScore = $nb;
    }

}