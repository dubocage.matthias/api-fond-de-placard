<?php

namespace App\Business\Models;

class User extends Model
{
    private string $pseudo;
    private string $password;

    function getPseudo():string{
        return $this->pseudo;
    }

    function getPassword():string{
        return $this->password;
    }

    function setPseudo(string $pseudo){
        return $this->pseudo = $pseudo;
    }

    function setPassword(string $password){
        $this->password = $password;
    }

}