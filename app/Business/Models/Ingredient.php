<?php

namespace App\Business\Models;

class Ingredient extends Model
{
    private $name;

    function getName():string{
        return $this->name;
    }

    function setName(string $name){
        return $this->name = $name;
    }

}