<?php

namespace App\Business;

use App\Business\DAOs\DAOIngredient;
use App\Business\DAOs\DAORecipe;
use App\Business\Models\Ingredient;
use App\Business\Models\Recipe;

class RecipeBusiness extends ModelBusiness
{
    /**
     * 
     */
    public function getAll():array
    {
        $daoRecipe = new DAORecipe($this->db);

        return $daoRecipe->getAllRecipe();
    }

    /**
     * 
     */
    public function insert(array $datas) : bool
    {
        $daoRecipe = new DAORecipe($this->db);
        $newRecipe = new Recipe();

        $newRecipe->hydrate($datas);
        $daoRecipe->insertRecipe($newRecipe);

        $ingredients = [];

        foreach($datas['ingredients'] as $ingredient_id)
        {
            $ingredients[] = new Ingredient($ingredient_id);
        }
        
        return $daoRecipe->ingredientsLinkedToRecipe($ingredients,$newRecipe);
    }

    /**
     * 
     */
    public function getIngredientByIdRecipe(int $id) : array
    {
        $datas =[ 'recipe' => null, 'ingredients' => null ];
        $daoRecipe = new DAORecipe($this->db);
        $daoIngredient = new DAOIngredient($this->db);

        $recipe = new Recipe($id);
        $daoRecipe->chargeRecipe($recipe);

        $ingredients = $daoIngredient->getIngredientsByIdRecipe($id);

        $datas['recipe'] = $recipe;
        $datas['ingredients'] = $ingredients;

        return $datas;
    }

    /**
     * 
     */
    public function search(array $words) : array
    {
        $daoRecipe = new DAORecipe($this->db);
        $recipes = [];

        foreach(explode(' ',$words['search']) as $word)
        {
            foreach ($daoRecipe->search($word) as $newRecipe)
            {
                $contain = false;
                foreach($recipes as $recipe)
                {
                    if($recipe->getId() === $newRecipe->getId())
                    {
                        $contain = true;
                    }
                }

                if($contain === false){
                    $recipes[] = $newRecipe;
                }  
            }
        }

        return $recipes;
    }

    /**
     * 
     */
    public function newScore(array $datas) : bool
    {
        $daoRecipe = new DAORecipe($this->db);
        $recipe = new Recipe($datas['id']);
        $daoRecipe->chargeRecipe($recipe);
        
        $totalScore = (($recipe->getScore() * $recipe->getNbScore()) + $datas['score']) / ($recipe->getNbScore() + 1);
        $recipe->setScore($totalScore);
        $recipe->setNbScore($recipe->getNbScore() + 1);
        return $daoRecipe->updateRecipe($recipe);
    }

    /**
     * 
     */
    public function update(array $datas) : bool
    {
        $daoRecipe = new DAORecipe($this->db);
        $newRecipe = new Recipe();

        $newRecipe->hydrate($datas);
        $daoRecipe->updateRecipe($newRecipe);
        
        $ingredients = [];
        
        $daoRecipe->deletedLinksRecipeIngredient($newRecipe);

        foreach($datas['ingredients'] as $ingredient_id)
        {
            $ingredients[] = new Ingredient($ingredient_id);
        }

        $daoRecipe->ingredientsLinkedToRecipe($ingredients,$newRecipe);

        return true;
    }
}