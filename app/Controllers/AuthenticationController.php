<?php

namespace App\Controllers;

use App\Business\AuthenticationBusiness;

class AuthenticationController extends Controller
{
    /**
     * 
     */
    public function connection()
    {
        return $this->view('auth.connection');
    }


    /**
     * 
     */
    public function validatedConnection()
    {
        $authBusiness = new AuthenticationBusiness($this->db);

        if($authBusiness->isValide($_POST['username'],$_POST['password']))
        {
            $_SESSION['tokenAuth'] = $authBusiness->getTokenAuth($_POST['username'],$_POST['password']);
            if($authBusiness->isAdmin()){
                return header('Location: /admin/home');
            }
            return header('Location: /home');
        }
        return header('Location: /connection');
    }

    /**
     * 
     */
    public function registration()
    {
        return $this->view('auth.registration');
    }

    /**
     * 
     */
    public function validatedRegistration()
    {

    }

    /**
     * 
     */
    public function deconnection()
    {

    }
}