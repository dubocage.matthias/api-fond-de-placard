<?php

namespace App\Controllers;

use Database\DBConnection;
use App\Business\IngredientBusiness;

class IngredientController extends Controller
{
    /**
     * 
     */
    public function index()
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $ingredients = $business->getAll();

        return $this->view('ingredient.ingredients',compact('ingredients',$ingredients));
    }

    /**
     * 
     */
    public function add()
    {
        return $this->view('ingredient.add');
    }

    /**
     * 
     */
    public function insert()
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $business->insert($_POST);

        return header('Location: /ingredients');
    }

    /**
     * 
     */
    public function show(int $id)
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $datas = $business->getRecipesByIdIngredient($id);
        
        $ingredient = $datas['ingredient'];
        $recipes = $datas['recipes'];

        return $this->view('ingredient.recipes', compact('ingredient','recipes'));
    }

    /**
     * 
     */
    public function searchWithIngredient()
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $recipes = $business->getRecipesByListIdIngredient($_POST);
        
        return $this->view('recipe.recipes',compact('recipes'));
    }
}