<?php

namespace App\Controllers\Admin;

use App\Controllers\Controller;

class HomeAdminController extends Controller
{
    public function index()
    {
        return $this->view('admin.home.index');
    }
}