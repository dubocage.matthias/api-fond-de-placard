<?php

namespace App\Controllers;

use App\Business\IngredientBusiness;
use Database\DBConnection;

class HomeController extends Controller
{
    /**
     * 
     */
    public function index()
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $ingredients = $business->getAll();

        return $this->view('home.index',compact('ingredients',$ingredients));
    }
}