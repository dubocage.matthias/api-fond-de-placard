<?php

namespace App\Controllers;

use App\Business\AuthenticationBusiness;

class Controller
{
    public function __construct()
    {
        if(session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
    }

    /**
     * 
     */
    public function view(string $path, array $params = null)
    {
        ob_start();
        $path = str_replace('.',DIRECTORY_SEPARATOR, $path);
        require VIEWS . $path . '.php';
        $content = ob_get_clean();
        require VIEWS . 'layout.php';
    }

    /**
     * 
     */
    public function isAuth()
    {
        $authBusiness = new AuthenticationBusiness($this->db);

        if(isset($_SESSION['authtoken']) && $authBusiness->tokenIsValide($_SESSION['authtoken']))
        {
            return true;
        }

        return header('Location: /connection');
    }
}