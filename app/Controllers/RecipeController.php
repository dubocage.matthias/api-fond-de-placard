<?php

namespace App\Controllers;

use App\Business\RecipeBusiness;
use App\Business\IngredientBusiness;
use Database\DBConnection;

class RecipeController extends Controller
{
    /**
     * 
     */
    public function index()
    {
        //$this->isAuth();
        $business = new RecipeBusiness(DBConnection::getInstance());
        $recipes = $business->getAll();
        
        return $this->view('recipe.recipes',compact('recipes'));
    }

    /**
     * 
     */
    public function add()
    {
        $business = new IngredientBusiness(DBConnection::getInstance());
        $ingredients = $business->getAll();

        return  count($ingredients) !== 0 ? $this->view('recipe.add',compact('ingredients')): header('Location: /recipes');
    }

    /**
     * 
     */
    public function insert()
    {
        $business = new RecipeBusiness(DBConnection::getInstance());
        $business->insert($_POST);

        return header('Location: /recipes');
    }

    /**
     * 
     */
    public function show(int $id)
    {
        $business = new RecipeBusiness(DBConnection::getInstance());
        $datas = $business->getIngredientByIdRecipe($id);

        $ingredients = $datas['ingredients'];
        $recipe = $datas['recipe'];

        return $this->view('recipe.show', compact('recipe','ingredients'));
    }

    /**
     * 
     */
    public function search()
    {
        $business = new RecipeBusiness(DBConnection::getInstance());
        $recipes =  $business->search($_POST);
        
        return $this->view('recipe.recipes', compact('recipes'));
    }

    /**
     * 
     */
    public function assess()
    {
        $business = new RecipeBusiness(DBConnection::getInstance());
        $business->newScore($_POST);
        
        $datas = $business->getIngredientByIdRecipe($_POST['id']);

        $ingredients = $datas['ingredients'];
        $recipe = $datas['recipe'];

        return $this->view('recipe.show', compact('recipe','ingredients'));
    }

    /**
     * 
     */
    public function edit($id)
    {
        $rBusiness = new RecipeBusiness(DBConnection::getInstance());
        $iBusiness = new IngredientBusiness(DBConnection::getInstance());
        $allIngredients = $iBusiness->getAll();

        $datas = $rBusiness->getIngredientByIdRecipe($id);

        $ingredients = $datas['ingredients'];
        $recipe = $datas['recipe'];

        return $this->view('recipe.edit', compact('recipe','ingredients','allIngredients'));
    }

    /**
     * 
     */
    public function update()
    {
        $business = new RecipeBusiness(DBConnection::getInstance());
        $business->update($_POST);

        $datas = $business->getIngredientByIdRecipe($_POST['id']);

        $ingredients = $datas['ingredients'];
        $recipe = $datas['recipe'];

        return $this->view('recipe.show', compact('recipe','ingredients'));
    }

}