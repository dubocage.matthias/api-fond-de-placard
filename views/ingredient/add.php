<h1>Ajouter un ingredient</h1>
<hr>
<form action="/ingredients/add" method="POST">

    <div class="form-group">
        <label for="name">Nom de l'ingredient *</label>
        <input type="text" class="form-control" id="name" name='name' placeholder="Entrez le nom de l'ingredient" required>
    </div>

    <hr>

    <button type="submit" class="btn btn-primary">Valider</button>

</form>