<div class="d-flex justify-content-between">
    <div class="text-center">
        <h1>Liste des ingredients</h1>
    </div>
    <div></div>
    <div class="d-flex align-items-center">
        <a href="/ingredients/add" class="btn btn-primary">Ajouter un ingredient</a>
    </div>
</div>
<div class="container" style="margin-top:50px">
    <table class="table">
        <tbody>
            <?php $i = 0;?>
                <?php foreach($params['ingredients'] as $ingredient){ ?>  
                    <?php if( $i%15 === 0 || $i === 0 ){?> <tr> <?php }?>
                    <?php if( $i%5 === 0  || $i === 0 ){?> <td> <?php }?>
                    <a href="/ingredient/<?= $ingredient->getId()?>" class="list-group-item list-group-item-action text-center" >
                        <b><?= $ingredient->getName()?></b>
                    </a>
                    <?php $i++;?>
                    <?php if( $i%5 === 0 || $i === 0){?> </td> <?php }?>
                    <?php if( $i%15 === 0 || $i === 0){?> </tr> <?php }?>
                <?php }?> 
                </td>
            </tr> 
        </tbody>
    </table>
</div>