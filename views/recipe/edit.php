<h1>Editer la recette</h1>
<hr>
<form action="/recipe/update" method="POST">

    <input id="id" name="id" type="hidden" value="<?=$params['recipe']->getId()?>">
    <input id="nbScore" name="nbScore" type="hidden" value="<?=$params['recipe']->getNbScore()?>">
    <input id="score" name="score" type="hidden" value="<?=$params['recipe']->getScore()?>">

    <div class="form-group">
        <label for="name">Nom de la recette * :</label>
        <input type="text" class="form-control" id="name" name='name' placeholder="Entrez le nom de la recette" required value="<?=$params['recipe']->getName()?>">
    </div>

    <div class="form-group">
        <label for="category">Catégorie * :</label>
        <input type="text" class="form-control" id="category" name='category' placeholder="Entrez le nom de la sa catégory" required value="<?=$params['recipe']->getCategory()?>">
    </div>

    <div class="form-group">
        <label for="picture">Lien de la photo :</label>
        <input type="link" class="form-control" id="picture" name='picture' placeholder="Entrez entrez le lien de la photo" value="<?=$params['recipe']->getPicture()?>">
    </div>

    <div class="form-group">
        <div class="container" style="margin-top:50px">
            <table class="table">
                <tbody>
                    <?php $i = 1;?>
                    <tr> 
                        <td>
                        <?php foreach($params['allIngredients'] as $ingredient){ ?>  
                            <?php if( $i%15 === 0 || $i === 0 ){?> <tr> <?php }?>
                            <?php if( $i%5 === 0  || $i === 0 ){?> <td> <?php }?>
                            <div class="form-check">
                                <input  class="form-check-input" type="checkbox" <?php  foreach($params['ingredients'] as $selected){
                                                                                            if($selected->getId() === $ingredient->getId())
                                                                                            {?>
                                                                                                checked
                                                                                            <?php } 
                                                                                        } ?> 
                                        value="<?= $ingredient->getId() ?>" id="<?= $ingredient->getName()."_".$ingredient->getId() ?>" name="ingredients[]">
                                <label class="form-check-label" for="<?= $ingredient->getName()."_".$ingredient->getId() ?>">
                                    <?= $ingredient->getName()?>
                                </label>
                            </div>
                            <?php $i++;?>
                            <?php if( $i%5 === 0 || $i === 0){?> </td> <?php }?>
                            <?php if( $i%15 === 0 || $i === 0){?> </tr> <?php }?>
                        <?php }?> 
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
    </div>

    <hr>

    <div class="text-center">
        <button type="submit" class="btn btn-primary">Valider</button>
    </div>

</form>