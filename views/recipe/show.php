<div class="text-center">
    <h1><?= $params['recipe']->getName()?></h1>
</div>

<hr>

<div class="text-center">
    <img class="rounded" src="<?= $params['recipe']->getPicture() !== '' ? $params['recipe']->getPicture() : "https://murviel-info-beziers.com/wp-content/uploads/2019/06/default-featured-150.png" ?>" style="max-height:200px;">
</div>

<div class="d-flex justify-content-center" style="margin-top:50px;">
    <div class="rating m-1">
        <div class="stars h1">
            <i class="fa fa-star <?php echo $params['recipe']->getScore()/1 >= 1 ? 'gold': ''?>">*</i>
            <i class="fa fa-star <?php echo $params['recipe']->getScore()/2 >= 1 ? 'gold': ''?>">*</i>
            <i class="fa fa-star <?php echo $params['recipe']->getScore()/3 >= 1 ? 'gold': ''?>">*</i>
            <i class="fa fa-star <?php echo $params['recipe']->getScore()/4 >= 1 ? 'gold': ''?>">*</i>
            <i class="fa fa-star <?php echo $params['recipe']->getScore()/5 >= 1 ? 'gold': ''?>">*</i>
        </div>
    </div>
    <form action="/recipe/assess" method="POST">
        <div class="d-flex justify-content-center">
            <input id="id" name="id" type="hidden" value="<?=$params['recipe']->getId()?>">
            <select class="custom-select m-1" name="score" id="score" required>
                <option value="5">5/5</option>
                <option value="4">4/5</option>
                <option value="5">3/5</option>
                <option value="2">2/5</option>
                <option value="1">1/5</option>
            </select>
            <button type="submit" class="btn btn-primary btn-sm m-1">Évaluer</button>
        </div>
    </form>
</div>

<div class="container" style="margin-top:50px;">

    <div class="text-center">
        <div class="h3">Les Ingredients de la recette :</div> 
    </div>
    <div class="container">
        <table class="table">
            <tbody>
                <?php $i = 1;?>
                <tr> 
                    <td>
                    <?php foreach($params['ingredients'] as $ingredient){ ?>  
                        <?php if( $i%15 === 0 || $i === 0 ){?> <tr> <?php }?>
                        <?php if( $i%5 === 0  || $i === 0 ){?> <td> <?php }?>
                        <a href="/ingredient/<?= $ingredient->getId()?>" class="list-group-item list-group-item-action text-center" >
                            <b><?= $ingredient->getName()?></b>
                        </a>
                        <?php $i++;?>
                        <?php if( $i%5 === 0 || $i === 0){?> </td> <?php }?>
                        <?php if( $i%15 === 0 || $i === 0){?> </tr> <?php }?>
                    <?php }?> 
                    </td>
                </tr> 
            </tbody>
        </table>
    </div>
</div>

<hr>

<div class="text-center m-4">
    <a href="/recipe/edit/<?=$params['recipe']->getId()?>" class="btn btn-primary">Proposer une modification</a>
</div>