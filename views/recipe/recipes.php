<h1>Liste des recettes</h1>
<hr>

<div class="container">
    <div class="row">
        <a href="/recipes/add" class="btn btn-primary col-2">Ajouter une recette</a>

        <form class="form-inline my-2 my-lg-0 col-5" action="/recipes/search" method="POST">
            <input class="form-control mr-sm-2" type="search" placeholder="Nom/categorie/ingredient" aria-label="Search" name="search" id="search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
    </div>
</div>
<div class="container" style="margin-top: 30px;">
    <table class="table">
        <thead>
            <tr>
                <th class="align-middle" scope="col"></th>
                <th class="align-middle" scope="col">Nom de la recette</th>
                <th class="align-middle" scope="col">Les categories</th>
                <th class="align-middle" scope="col">La note</th>
                <th class="align-middle" scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($params['recipes'] as $recipe){ ?> 
                <tr>
                    <td class="align-middle"><img src="<?= $recipe->getPicture() !== ''? $recipe->getPicture() : "https://murviel-info-beziers.com/wp-content/uploads/2019/06/default-featured-150.png" ?>" style="max-height:100px;max-width:100px"></td>
                    <td class="align-middle"><?= $recipe->getName()?></td>
                    <td class="align-middle"><?= $recipe->getCategory()?></td>
                    <td class="align-middle">
                        <div class="rating ">
                                <div class="stars h4">
                                <i class="fa fa-star <?php echo $recipe->getScore()/1 >= 1 ? 'gold': ''?>">*</i>
                                <i class="fa fa-star <?php echo $recipe->getScore()/2 >= 1 ? 'gold': ''?>">*</i>
                                <i class="fa fa-star <?php echo $recipe->getScore()/3 >= 1 ? 'gold': ''?>">*</i>
                                <i class="fa fa-star <?php echo $recipe->getScore()/4 >= 1 ? 'gold': ''?>">*</i>
                                <i class="fa fa-star <?php echo $recipe->getScore()/5 >= 1 ? 'gold': ''?>">*</i>
                            </div>
                        </div>
                    </td>
                    <td class="align-middle"><a href="/recipe/<?= $recipe->getId()?>" class="btn btn-primary">Voir la recette</a></td>
                </tr> 
            <?php } ?> 
        </tbody>
    </table>
</div>