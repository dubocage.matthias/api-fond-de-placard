<h1>Ajouter une recette</h1>
<hr>
<form action="/recipes/add" method="POST">

    <div class="form-group">
        <label for="name">Nom de la recette * :</label>
        <input type="text" class="form-control" id="name" name='name' placeholder="Entrez le nom de la recette" required>
    </div>

    <div class="form-group">
        <label for="category">Catégorie * :</label>
        <input type="text" class="form-control" id="category" name='category' placeholder="Entrez le nom de la sa catégory" required>
    </div>

    <div class="form-group">
        <label for="picture">Lien de la photo :</label>
        <input type="link" class="form-control" id="picture" name='picture' placeholder="Entrez entrez le lien de la photo">
    </div>

    <div class="form-group">
        <div class="container" style="margin-top:50px">
            <table class="table">
                <tbody>
                    <?php $i = 1;?>
                    <tr> 
                        <td>
                        <?php foreach($params['ingredients'] as $ingredient){ ?>  
                            <?php if( $i%15 === 0 || $i === 0 ){?> <tr> <?php }?>
                            <?php if( $i%5 === 0  || $i === 0 ){?> <td> <?php }?>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="<?= $ingredient->getId() ?>" id="<?= $ingredient->getName()."_".$ingredient->getId() ?>" name="ingredients[]">
                                <label class="form-check-label" for="<?= $ingredient->getName()."_".$ingredient->getId() ?>">
                                    <?= $ingredient->getName()?>
                                </label>
                            </div>
                            <?php $i++;?>
                            <?php if( $i%5 === 0 || $i === 0){?> </td> <?php }?>
                            <?php if( $i%15 === 0 || $i === 0){?> </tr> <?php }?>
                        <?php }?> 
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
    </div>

    <hr>

    <button type="submit" class="btn btn-primary">Valider</button>

</form>