<h1>Connection</h1>

<form action="/connection" method="POST">

  <div class="form-group">
    <label for="username">Pseudo</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="Entrez votre pseudo">
  </div>

  <div class="form-group">
    <label for="password">Mot de passe</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Entre votre mot de passe">
  </div>

  <button type="submit" class="btn btn-primary">Se Connecter</button>

</form>

<a href="/registration" class="btn btn-primary">S'inscrire</a>