<div class="text-center">
    <h1>Fond de placard</h1>
</div>
<hr>
<div class="text-center">
    <h4>
        <p>Bonjour, bienvenue sur Fond de placard !</p>
        <p>Cette application a été créee pour vous aider à trouver une recette à partir d'ingrèdients présents dans votre réfrégirateur et dont vous ne savez pas quoi faire !</p>
        <p>Deux options sont possibles. Vous pouvez tout d'abord sélectionner les ingredients ci-dessous et débuter une recherche. </p>
        <p>Ou bien regarder uniquement la liste des recettes.</p>
        <p>Bon appétit !</p>
    </h4>
</div>
<hr>
<div class="container" style="margin-top:50px">
    <form action="/ingredients/search/recipes" method="POST">
        <div class="text-center">
            <h2>Sélectionner les ingrédients </h2>
        </div>
        <div class="container" style="margin-top:30px">
            <table class="table">
                <tbody>
                    <?php $i = 1;?>
                    <tr> 
                        <td>
                        <?php foreach($params['ingredients'] as $ingredient){ ?>  
                            <?php if( $i%15 === 0 || $i === 0 ){?> <tr> <?php }?>
                            <?php if( $i%5 === 0  || $i === 0 ){?> <td> <?php }?>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="<?= $ingredient->getId() ?>" id="<?= $ingredient->getName()."_".$ingredient->getId() ?>" name="ingredients[]">
                                <label class="form-check-label" for="<?= $ingredient->getName()."_".$ingredient->getId() ?>">
                                    <?= $ingredient->getName()?>
                                </label>
                            </div>
                            <?php $i++;?>
                            <?php if( $i%5 === 0 || $i === 0){?> </td> <?php }?>
                            <?php if( $i%15 === 0 || $i === 0){?> </tr> <?php }?>
                        <?php }?> 
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
        <hr> 

        <div class="text-center">
            <button type="submit" class="btn btn-primary" style="margin-bottom:30px;">Valider</button>
        </div>
    </form>
</div>