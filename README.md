# api-fond-de-placard

J'ai commencer à monter un framework "maison" et j'ai voulue l'utiliser pour mieux appréhender le sujet !

# Les Dossiers du projet

    - app:
        - Dans le dossier business : contient l'ensemble des classes business et les dossiers suivant
            - Dans le dossier DAO : nous avons les différentes class Dao de nos Models et une interface DBConnection.
            - Dans le dossier Model nous avons les différentes class Model lié a nos table de base de donnée.

        - Dans le dossier Controller : contient l'ensemble des classes Controller
        - Dans le dossier Exceptions : **ne pas s'en occuper**
        - Dans le dossier Validators : **ne pas s'en occuper**

    - database : contient le fichier de connexion a la base de donnée (seulement MYSQ pour l'instant)

    - public : point d'entrer !

    - route : contient la logique de routage de l'application

    - vendor : **ne pas s'en occuper**

    - views:
        - admin : **ne pas s'en occuper**
        - auth : **ne pas s'en occuper**
        - errors : contient les pages d'errors
        - home :  contient la page principal de l'application
        - ingredient : contient l'ensemble des pages lié à ingredient
        - recipe : contient l'ensemble des pages lié à recette

# La Base de donnée

    - Tables recipe : la table ou serons stocker les recettes
        1. id
        2. name
        3. category
        4. picture
        5. score
        6. nbScore

    - Tables ingredient : la table ou serons stocker les ingredients
        1. id
        2. name

    - Tables recipe_ingredient : la table de jointure entre recipe et ingredient
        1. id
        2. recipe_id
        3. ingredient_id