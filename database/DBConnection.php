<?php

namespace Database;

use App\Business\DAOs\Interfaces\DBConnectionInterface;
use App\Exceptions\NotCloneException;
use Exception;
use PDO;

class DBConnection implements DBConnectionInterface
{  
    private const DEFAULT_SQL_USER = 'root';
    private const DEFAULT_SQL_HOST = '127.0.0.1';
    private const DEFAULT_SQL_PASS = '';
    private const DEFAULT_SQL_DTB = 'home_framework';

    private $pdo = null;
    private static $instance = null;

    /**
     * 
     */
    private function __construct()
    {
        $this->pdo = new PDO(
            "mysql:dbname=".self::DEFAULT_SQL_DTB.";host=".self::DEFAULT_SQL_HOST,
            self::DEFAULT_SQL_USER,
            self::DEFAULT_SQL_PASS,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET UTF8',
            ]
        );
    }
    /**
     * 
     */
    public static function getInstance():DBConnectionInterface
    {
        if(is_null(self::$instance))
        {
          self::$instance = new DBConnection();
        }
        return self::$instance;
    }


    /**
     * 
     */
    public function query(string $query, string $fetch = 'fetchAll'):array
    {
        try
        {
            $req = $this->pdo->query($query);
            $req->execute();

            return $fetch === 'fetch' ? $req->fetch() :  $req->fetchAll();

        }catch(Exception $e)
        {

        }
        return [];
    }

    /**
     * 
     */
    public function prepare(string $query, array $values , string $fetch = 'fetchAll',bool $insert = false):array
    {
        try
        {
            $req = $this->pdo->prepare($query);
            foreach($values as $value)
            {
                $req->bindValue($value['param'],$value['value'],$this->getTypePDO($value['type']));
            }
            $req->execute();
            
            if($insert){
                return ['id' => $this->pdo->lastInsertId()];
            }

            return $fetch === 'fetch' ? $req->fetch() :  $req->fetchAll();

        }catch(Exception $e)
        {

        }
        return [];
    }

    /** 
    * 
    */
    private function getTypePDO(string $value = "string") : int
    {
        switch($value)
        {
            case 'string':
                $type = PDO::PARAM_STR;
                break;
            case 'int':
                $type = PDO::PARAM_INT;
                break;
            case 'float':
                $type = PDO::PARAM_STR;
                break;  
            default:
                $type = PDO::PARAM_STR;
            break;
        }
        
        return $type;
    }

    /**
     * 
     */
    public function __clone(){
        throw new NotCloneException('Cet objet ne peut pas être cloné');
    }

}