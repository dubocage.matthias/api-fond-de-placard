<?php

require '../vendor/autoload.php';

use App\Exceptions\NotFoundException;
use Router\Router;

define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME'] . DIRECTORY_SEPARATOR ));

$router = new Router($_GET['url']);

/*----------authentication----------*/
$router->get(   '/connection'   ,   'App\Controllers\AuthenticationController@connection');
$router->post(  '/connection'   ,   'App\Controllers\AuthenticationController@validatedConnection');
$router->get(   '/registration' ,   'App\Controllers\AuthenticationController@registration');
$router->post(  '/registration' ,   'App\Controllers\AuthenticationController@validatedRegistration');
$router->get(   '/deconnection' ,   'App\Controllers\AuthenticationController@deconnection');

/*----------Admin----------*/
$router->get(   '/admin/home'   ,   'App\Controllers\Admin\HomeAdminController@index');

/*----------Home----------*/
$router->get(   '/home'         ,   'App\Controllers\HomeController@index');
$router->get(   '/'             ,   'App\Controllers\HomeController@index');

/*--------Ingredient------*/
$router->get(   '/ingredients'                  ,   'App\Controllers\IngredientController@index');
$router->get(   '/ingredients/add'              ,   'App\Controllers\IngredientController@add');
$router->post(  '/ingredients/add'              ,   'App\Controllers\IngredientController@insert');
$router->get(   '/ingredient/:id'               ,   'App\Controllers\IngredientController@show');
$router->post(  '/ingredients/search/recipes'   ,   'App\Controllers\IngredientController@searchWithIngredient');

/*---------Recipe---------*/
$router->get(   '/recipes'                      ,   'App\Controllers\RecipeController@index');
$router->get(   '/recipes/add'                  ,   'App\Controllers\RecipeController@add');
$router->post(  '/recipes/add'                  ,   'App\Controllers\RecipeController@insert');
$router->get(   '/recipe/:id'                   ,   'App\Controllers\RecipeController@show');
$router->post(  '/recipes/search'               ,   'App\Controllers\RecipeController@search');
$router->post(  '/recipe/assess'                ,   'App\Controllers\RecipeController@assess');
$router->get(   '/recipe/edit/:id'              ,   'App\Controllers\RecipeController@edit');
$router->post(  '/recipe/update'                ,   'App\Controllers\RecipeController@update');

try{
    $router->run();
}catch (NotFoundException $e){
    echo $e->error404();
}